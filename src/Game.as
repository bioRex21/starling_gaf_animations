package 
{
	import com.catalystapps.gaf.core.ZipToGAFAssetConverter;
	import com.catalystapps.gaf.data.GAFBundle;
	import com.catalystapps.gaf.data.GAFTimeline;
	import com.catalystapps.gaf.display.GAFMovieClip;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.utils.ByteArray;
    import starling.animation.Transitions;
    import starling.core.Starling;
    import starling.display.Image;
    import starling.events.TouchEvent;
    import starling.events.TouchPhase;
    import starling.utils.deg2rad;

    /** The Game class represents the actual game. In this scaffold, it just displays a
     *  Starling that moves around fast. When the user touches the Starling, the game ends. */ 
    public class Game extends Scene
    {
		

		[Embed(source="../bin/assets/witch.zip", mimeType="application/octet-stream")]
		private const FiremanZip: Class;
		
		
        public static const GAME_OVER:String = "gameOver";
        
        private var _bird:Image;

        public function Game()
        { }
        
        override public function init(width:Number, height:Number):void
        {
            super.init(width, height);

			
            _bird = new Image(Root.assets.getTexture("starling_rocket"));
            _bird.pivotX = _bird.width / 2;
            _bird.pivotY = _bird.height / 2;
            _bird.x = width / 2;
            _bird.y = height / 2;
            _bird.addEventListener(TouchEvent.TOUCH, onBirdTouched);
            addChild(_bird);
            
            moveBird();
			trace("zip");
			var zip: ByteArray = new FiremanZip();
			
			var converter: ZipToGAFAssetConverter = new ZipToGAFAssetConverter();
			trace("instance");
			converter.addEventListener(Event.COMPLETE, this.onConverted);
			converter.addEventListener(ErrorEvent.ERROR, this.onError);
			trace("convert");
			converter.convert(zip);
        }
		
		private function onConverted(event: Event): void
		{
			
			var gafBundle: GAFBundle = (event.target as ZipToGAFAssetConverter).gafBundle;
			
			
			//witch is the swf name, boy_anim the movieclips linkage name
			var gafTimeline: GAFTimeline = gafBundle.getGAFTimeline("witch","boy_anim");// boy_anim witch_anim, cat_anim
			
			var mc: GAFMovieClip = new GAFMovieClip(gafTimeline);
			

			this.addChild(mc);
			mc.play();
			
			gafTimeline= gafBundle.getGAFTimeline("witch","cat_anim");// boy_anim witch_anim, cat_anim
			
			var mc2: GAFMovieClip = new GAFMovieClip(gafTimeline);
			

			this.addChild(mc2);
			mc2.play();
			
			
			mc.x = 200;
			mc.y = 200;
			
			mc2.x = 200;
			mc2.y = 500;
			
			mc2.scale = 0.5;			
		}

		private function onError(event: ErrorEvent): void
		{
			trace(event);
		}

        private function moveBird():void
        {
            var scale:Number = Math.random() * 0.8 + 0.2;
            
            Starling.juggler.tween(_bird, Math.random() * 0.5 + 0.5, {
                x: Math.random() * _width,
                y: Math.random() * _height,
                scaleX: scale,
                scaleY: scale,
                rotation: Math.random() * deg2rad(180) - deg2rad(90),
                transition: Transitions.EASE_IN_OUT,
                onComplete: moveBird
            });
        }
        
        private function onBirdTouched(event:TouchEvent):void
        {
            if (event.getTouch(_bird, TouchPhase.BEGAN))
            {
                Root.assets.playSound("click");
                Starling.juggler.removeTweens(_bird);
                dispatchEventWith(GAME_OVER, true, 100);
            }
        }
    }
}