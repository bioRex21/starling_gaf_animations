# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Using tech ###

Uses Gamua's scaffold mobile from samples at [https://github.com/Gamua/Starling-Framework/tree/master/samples]
from repository [https://bitbucket.org/bioRex21/starling_builder_and_scaffold_mobile]
	
GAF Player RC for Starling 2.1
[https://github.com/CatalystApps/StarlingGAFPlayer/tree/rc_starling_2.1]
	
For 2x animations, it's better towork on .fla with the 2x size from the start. Then GAF will create 1x versions (See screenshots gaf_config.png and gaf_config_2.png).


### How do I get set up? ###

- Create a swf from fla.

- Drag SWF to GAF Converter [https://gafmedia.com/downloads]
	
- Will automatically create a .zip  in the same folder as the .swf

- Embed that .zip and use it in project

If compile to device fails (something about "dx tool" and/or specify correct AIR version), make sure to add the *-Xms512m -Xmx512m* parameters to adt.bat  in /bin of the Flex SDK, so;
[at]java -jar -Xms512m -Xmx512m "%~dp0\..\lib\adt.jar" %*